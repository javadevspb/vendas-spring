package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoVendasApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoVendasApiApplication.class, args);
	}
}
